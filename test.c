#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);
float max(float array[], int size);
float min(float array[], int size);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	char *humchar="h";
	char *tempchar="t";
	int counter=0;
	int baudrate = 9600;  // default
	char buff[3];
	float temp[12];
	float hum[12];
	float medtemp;
	float medhum;
	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	serialport_flush(fd);

	while(1)
	{
		write(fd, tempchar, 1);
		usleep(2500000);
		read(fd, buff, 1);
		temp[counter]=buff[0];
		printf("Temperatura: %d\n", buff[0]);
		write(fd, humchar, 1);
		usleep(2500000);
		read(fd, buff, 1);
		hum[counter]=buff[0];
		printf("Humedad: %d\n", buff[0]);
		if(counter==11)
		{	
			medtemp=calculateSD(temp);
			medhum=calculateSD(hum);
			printf("La media de la temperatura es: %.2f y su desviacion es: %.2f\n" ,medtemp, pow(medtemp,2));
			printf("El maximo de temperatura es: %.2f y el minimo de temperatura es: %.2f\n" , max(temp,12), min(temp,12) );
			printf("La media de la humedad es: %.2f y su desviacion es: %.2f\n" ,medhum, pow(medhum,2));
			printf("El maximo de humedad es: %.2f y el minimo de humedad es: %.2f\n" , max(hum,12), min(hum,12) );
			
			counter=-1;
		}
		counter++;	
	}

	
	close( fd );
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}



float max(float array[], int size){
	float max;
	max = array[0];
	
	for (int c = 0; c < size; c++){
		if (array[c] > max){
			max  = array[c];
		}
	}
	return max;
}

float min(float array[], int size){
	float min;
	min = array[0];
	
	for (int c = 0; c < size; c++){
		
		if (array[c] < min){
			
			min  = array[c];
		}
	}
	return min;
}


